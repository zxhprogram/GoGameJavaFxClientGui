package com.go_game.gui.response;

import java.util.Objects;

public class ConnectResponse {
    private boolean successful = false;
    private String err = null;

    public boolean isSuccessful() {
        if (err == null) {
            return successful;
        }
        return Objects.equals(err, "Already connected");
    }
}
