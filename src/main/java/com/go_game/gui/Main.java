package com.go_game.gui;

import com.go_game.gui.Game.Game;
import com.go_game.gui.Game.PlayerColor;
import com.go_game.gui.Game.Tile;
import com.go_game.gui.request.ConnectRequest;
import com.go_game.gui.request.GetStatusRequest;
import com.go_game.gui.request.MoveRequest;
import com.go_game.gui.response.ConnectResponse;
import com.go_game.gui.response.MoveResponse;
import com.go_game.gui.response.StatusResponse;
import com.google.gson.Gson;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;


public class Main extends Application {

    private String login = null;
    private PlayerColor color = null;
    private Label whitePlayerLoginLabel = new Label();
    private Label blackPlayerLoginLabel = new Label();
    private Button refreshButton = new Button("Refresh");

    private DataInputStream in;
    private DataOutputStream out;
    private ImageView mainBoardView;
    private Image whiteStoneImage;
    private Image blackStoneImage;
    private int scaleY = 30;
    private int scaleX = 31;
    private Pane mainPane;
    private Game currentStatus;
    private int step = 15;
    private Label scoreInfoLabel = new Label("Score");
    private Label scoreLabel = new Label();

    public static void main(String[] args) {
        launch(args);
    }

    private void initNetwork(String address, int port) throws IOException {
        InetAddress ipAddress = InetAddress.getByName(address);
        Socket socket = new Socket(ipAddress, port);
        InputStream sin = socket.getInputStream();
        OutputStream sout = socket.getOutputStream();
        in = new DataInputStream(sin);
        out = new DataOutputStream(sout);
    }

    private void init_views() {
        Image mainBackgroundImage = new Image(new File("resources/Blank_Go_board.png").toURI().toString(), 600, 575, false, false);
        mainBoardView = new ImageView();
        mainBoardView.setImage(mainBackgroundImage);
        whiteStoneImage = new Image(new File("resources/White_Go_Stone.png").toURI().toString(), 25, 25, false, false);
        blackStoneImage = new Image(new File("resources/Black_Go_Stone.png").toURI().toString(), 25, 25, false, false);
    }

    private boolean Connect(String color, String login) {
        try {
            initNetwork("127.0.0.1", 32444);
            ConnectRequest connectRequest = new ConnectRequest(login, color);
            out.write(connectRequest.WithPrefix());
            String s = new String(in.readAllBytes(), "US-ASCII");
            System.out.println("s = " + s);
            Gson gson = new Gson();
            ConnectResponse connectResponse = gson.fromJson(s, ConnectResponse.class);
            return connectResponse.isSuccessful();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }


    private Game getStatus(String login) {
        try {
            initNetwork("127.0.0.1", 32444);
            GetStatusRequest getStatusRequest = new GetStatusRequest(login);
            out.write(getStatusRequest.WithPrefix());
            String s = new String(in.readAllBytes(), "US-ASCII");
            Gson gson = new Gson();
            StatusResponse statusResponse = gson.fromJson(s, StatusResponse.class);
            return statusResponse.getStatus();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    class Indexes {
        int x = 0;
        int y = 0;
    }

    private Indexes convertPaneCoordsToIndecies(int x, int y) {
        int x_real = x - 8;
        int y_real = y - 30;
        Indexes result = new Indexes();
        int i;
        for (i = 0; i < 19; i++) {
            if (Math.abs(i * scaleX - x_real) < step) {
                result.x = i;
                break;
            }
        }
        if (i == 19) {
            return null;
        }
        for (i = 0; i < 19; i++) {
            if (Math.abs(i * scaleY - y_real) < step) {
                result.y = i;
                break;
            }
        }
        if (i == 19) {
            return null;
        }
        return result;
    }

    private MoveResponse doMove(String login, int x, int y) {
        try {

            Indexes converted = convertPaneCoordsToIndecies(x, y);
            if (converted != null) {
                initNetwork("127.0.0.1", 32444);
                MoveRequest moveRequest = new MoveRequest(login, converted.x, converted.y);
                out.write(moveRequest.WithPrefix());
                String s = new String(in.readAllBytes(), "US-ASCII");
                Gson gson = new Gson();
                return gson.fromJson(s, MoveResponse.class);
            }
            System.err.println("Cant parse click");
            return null;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private ImageView[][] getUpdatedBoardViews() {
        Tile[][] tiles = currentStatus.getBoard().getTiles();

        ImageView[][] board = new ImageView[tiles.length][tiles.length];
        for (int i = 0; i < tiles.length; i++) {
            for (int j = 0; j < tiles[i].length; j++) {
                if (tiles[i][j].equals(Tile.Black)) {
                    board[i][j] = new ImageView();
                    board[i][j].setImage(blackStoneImage);
                    board[i][j].setLayoutX(8 + j * scaleX);
                    board[i][j].setLayoutY(30 + i * scaleY);
                } else if (tiles[i][j].equals(Tile.White)) {
                    board[i][j] = new ImageView();
                    board[i][j].setImage(whiteStoneImage);
                    board[i][j].setLayoutX(8 + j * scaleX);
                    board[i][j].setLayoutY(30 + i * scaleY);
                } else {
                    board[i][j] = null;
                }
            }
        }
        return board;
    }

    private void setPositionOfControlElements() {
        refreshButton.setLayoutX(0);
        refreshButton.setLayoutY(0);
        mainBoardView.setLayoutX(0);
        mainBoardView.setLayoutY(25);
        whitePlayerLoginLabel.setLayoutX(100);
        whitePlayerLoginLabel.setLayoutY(0);
        blackPlayerLoginLabel.setLayoutX(450);
        blackPlayerLoginLabel.setLayoutY(0);
        scoreInfoLabel.setLayoutX(250);
        scoreInfoLabel.setLayoutY(0);
        scoreLabel.setLayoutX(300);
        scoreLabel.setLayoutY(0);
    }

    private void updateStatus() {
        currentStatus = getStatus(login);
        whitePlayerLoginLabel.setText("White: " + currentStatus.getWhite_player().getLogin());
        blackPlayerLoginLabel.setText("Black: " + currentStatus.getBlack_player().getLogin());
        scoreLabel.setText((currentStatus.getWhite_player().getScore() - currentStatus.getWhite_player().getPenalty())
                + " | "
                + (currentStatus.getBlack_player().getScore() - currentStatus.getBlack_player().getPenalty()));
    }

    private void redrawBoard() {
        updateStatus();
        mainPane.getChildren().clear();
        mainPane.getChildren().addAll(refreshButton, mainBoardView, whitePlayerLoginLabel, blackPlayerLoginLabel, scoreInfoLabel, scoreLabel);
        scoreLabel.setText((currentStatus.getWhite_player().getScore() - currentStatus.getWhite_player().getPenalty())
                + " | "
                + (currentStatus.getBlack_player().getScore() - currentStatus.getBlack_player().getPenalty()));
        ImageView[][] board = getUpdatedBoardViews();
        for (ImageView[] row : board) {
            for (ImageView item : row) {
                if (item != null) {
                    mainPane.getChildren().add(item);
                }
            }
        }
    }

    private void startUpdateCycle() {
        Timeline timeline = new Timeline(new KeyFrame(
                Duration.millis(2500),
                event -> {
                    redrawBoard();
                }
        ));
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }

    private Scene getMainScene() {
        updateStatus();
        startUpdateCycle();
        setPositionOfControlElements();
        mainPane = new Pane();
        refreshButton.setOnAction(event -> {
            redrawBoard();
        });
        mainPane.getChildren().addAll(refreshButton, mainBoardView, whitePlayerLoginLabel, blackPlayerLoginLabel, scoreInfoLabel, scoreLabel);
        Scene mainScene = new Scene(mainPane, 600, 600);
        mainPane.setOnMouseClicked(event -> {
            MoveResponse moveResponse = doMove(login, (int) event.getSceneX(), (int) event.getSceneY());
            if (moveResponse != null) {
                System.out.println("moveResponse.isValid() = " + moveResponse.isValid());
                redrawBoard();
            }
            redrawBoard();
        });
        return mainScene;
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        init_views();
        primaryStage.setTitle("Hola");
        primaryStage.setResizable(false);
        Scene scene = new Scene(new Group(), 600, 600, Color.WHITE);
        Group root = (Group) scene.getRoot();
        Label colorLabel = new Label("Color: ");
        Label loginLabel = new Label("Login: ");
        Label connectionStatusLabel = new Label();
        TextField loginTextField = new TextField();
        Button connectButton = new Button("Connect");

        final ComboBox colorSelector = new ComboBox();

        colorSelector.getItems().addAll(
                "White",
                "Black"
        );
        colorSelector.setValue("Black");

        connectButton.setOnAction(e -> {
            if (Connect(colorSelector.getValue().toString(), loginTextField.getText())) {
                login = loginTextField.getText();
                if (colorSelector.getValue().toString().equals("White")) {
                    color = PlayerColor.White;
                } else {
                    color = PlayerColor.Black;
                }
                Scene mainScene = getMainScene();
                primaryStage.setScene(mainScene);
            } else {
                connectionStatusLabel.setText("Can't connect");
            }
        });

        GridPane grid = new GridPane();
        grid.setVgap(4);
        grid.setHgap(10);
        grid.setPadding(new Insets(5, 5, 5, 5));
        grid.add(colorLabel, 0, 0);
        grid.add(loginLabel, 0, 1);
        grid.add(colorSelector, 1, 0);
        grid.add(connectButton, 2, 0);
        grid.add(loginTextField, 1, 1);
        grid.add(connectionStatusLabel, 3, 0);
        root.getChildren().add(grid);


        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
