package com.go_game.gui.request;

import com.google.gson.Gson;

import java.nio.charset.StandardCharsets;

public class MoveRequest {
    private String login;
    private int x;
    private int y;


    public MoveRequest(String login, int x, int y) {
        this.login = login;
        this.x = x;
        this.y = y;
    }

    public byte[] WithPrefix(){
        Gson gson = new Gson();
        String moveRequest = gson.toJson(this);
        return "Move ".concat(moveRequest).concat("\n").getBytes(StandardCharsets.UTF_8);
    }

}
