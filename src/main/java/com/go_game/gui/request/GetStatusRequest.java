package com.go_game.gui.request;

import com.google.gson.Gson;

import java.nio.charset.StandardCharsets;

public class GetStatusRequest {
    String login;

    public GetStatusRequest(String login) {
        this.login = login;
    }

    public String getLogin() {
        return login;
    }

    public byte[] WithPrefix(){
        Gson gson = new Gson();
        String getStatusRequest= gson.toJson(this);
        return "GetStatus ".concat(getStatusRequest).concat("\n").getBytes(StandardCharsets.UTF_8);
    }
}
